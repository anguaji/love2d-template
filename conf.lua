function love.conf(t)
    t.title = "Your project name"        -- The title of the window the game is in (string)
    t.author = "Anguaji"        -- The author of the game (string)
    t.url = "http://anguaji.net"                 -- The website of the game (string)
    t.screen.fullscreen = false -- Enable fullscreen (boolean)
end